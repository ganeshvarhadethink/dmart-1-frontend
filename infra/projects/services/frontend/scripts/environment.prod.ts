export const environment = {
  production: "ENV_PB_PRODUCTION",
  apiUrl:"ENV_PB_APIURL",
  client_id:"ENV_PB_CLIENT_ID",
  client_secret:"ENV_PB_CLIENT_SECRET"
};