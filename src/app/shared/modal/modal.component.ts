import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ApiService } from "../service/api.service";
import { DomSanitizer } from "@angular/platform-browser";
import { AuthService } from 'src/app/modules/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"]
})
export class ModalComponent implements OnInit {
  @Input("inputAppData") inputAppData: any;

  fileUrl: string;
  imageSource: any = [];
  base64Url :string;
  extension:string;
  fileName:string;
  appChecked = false;
  scytheChecked = false;
  isLoggedIn : boolean;

  constructor(
    private apiService: ApiService,
    private sanitizer: DomSanitizer,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.isAuthenticated();
    this.fileUrl = 'http:'+this.inputAppData.customData.icon;
    this.fileName = this.inputAppData.name;
    async function getBase64ImageFromUrl(imageUrl) {
      var res = await fetch(imageUrl);
      var blob = await res.blob();
    
      return new Promise((resolve, reject) => {
        var reader  = new FileReader();
        reader.addEventListener("load", function () {
            resolve(reader.result);
        }, false);
    
        reader.onerror = () => {
          return reject(this);
        };
        reader.readAsDataURL(blob);
      })
    }

    getBase64ImageFromUrl(this.fileUrl)
    .then((result:any) =>{ 
      this.base64Url = result
      this.extension = undefined;
      var lowerCase = this.base64Url.toLowerCase();
    if(lowerCase.indexOf("png") != -1)
    this.extension = "png";
    else if(lowerCase.indexOf("jpg") != -1 || lowerCase.indexOf("jpeg") != -1)
    this.extension = "jpg";
    }
      )
    .catch(err => console.error(err));

  }

  getDownloadURL(binaryValue: string) {
    this.apiService.getFileDownlaodURL(binaryValue).subscribe((data: any) => {
      this.fileUrl = data.url;
    });
  }

  isAuthenticated(){
  this.auth.isAuthenticated()
  .subscribe(
    result => {
      this.isLoggedIn = result;  
    },
    error => {
      console.log(error);
   });
  }

  isAppChecked(event: any){
    this.appChecked = !this.appChecked;
  }

  isScytheChecked(event: any){
    this.scytheChecked = !this.scytheChecked;
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  
}
