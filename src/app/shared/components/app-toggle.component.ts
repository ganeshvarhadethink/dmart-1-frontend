import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './app-toggle.component.html',
  styleUrls: ['./app-toggle.component.scss']
})
export class AppToggleComponent {
  @Input() isChecked: boolean = false;
  @Output() changeState = new EventEmitter<boolean>();

  public handleCheckedState(state) {
    this.isChecked = state;
    this.changeState.emit(state);
  }
}
